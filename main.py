print(
    """
Введите с клавиатуры строки, содержащие информацию о фильме, одну за одной 
ФОРМАТ: название (год выпуска в скобках) режиссер, жанр

Пример строки: 
Властелин колец (2003) Майкл Бэй, фэнтези Кузнец сердец (2033) Байкл Мэй, Экшн

Фильмы, введённые вне формата учитываться не будут!
    """
)
s = input()
content = [elem.strip() for elem in s.split(",")]

years = [
    int(elem[elem.find("(") + 1 : elem.find(")")])
    for elem in content
    if elem.find("(") >= 0 and elem.find(")") >= 0
]
authors = set(
    [
        elem[elem.rfind(")") + 1 :].strip()
        for elem in content
        if elem.rfind(")") >= 0 and elem.rfind("(") >= 0
    ]
)

if years:
    max_year = max(years)
    min_year = min(years)

    if min_year != max_year:
        print(f"\nСтарейший: {min_year}, Новейший: {max_year}")
    elif min_year == max_year:
        print(f"\nГоды фильмов совпадают: {max_year}")
else:
    print()

if authors:
    print("Режиссёры:", ", ".join(authors))
elif len(authors) == 1:
    print("Режиссёр: ", authors)
